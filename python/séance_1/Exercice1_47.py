def commun(str1,str2):
    commun=[]
    str1=str1.split(" ")
    str2=str2.split(" ")
    for i in str1:
        if( i in str2 and i not in commun):
            commun.append(i)
    
    return commun

s1 = "kevin s'est pris une amende car il a pas payé le parking"
s2 = "adrien n'a pas pris une amende et a trouvé 23 certificats"

commun = commun(s1,s2)

print(commun)