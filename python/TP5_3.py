class TableMultiplication:
    def __init__(self,valeurs):
        self.value=valeurs
        self.rank=0
    
    def prochain(self):
        print(self.value*self.rank)
        self.rank+=1
    
    def __repr__(self):
        return repr("table de multiplication de "+str(self.value)+" au rank "+str(self.rank-1))

tab=TableMultiplication(3)
tab.prochain()
tab.prochain()
tab.prochain()
print(repr(tab))