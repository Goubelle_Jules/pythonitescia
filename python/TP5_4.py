import math
class Fraction:
    def __init__(self,a,b):
        self.num=a
        self.denom=b
    
    def affiche(self):
        print(str(self.num)+"/"+str(self.denom))

    def __add__(self,o):
        pgcd=math.gcd(self.denom,o.denom)
        firstdiv=self.denom/pgcd
        seconddiv=o.denom/pgcd

        numT=self.num/firstdiv+o.num/seconddiv

        return str(numT)+"/"+str(pgcd)

    def __sub__(self,o):
        pgcd=math.gcd(self.denom,o.denom)
        firstdiv=self.denom/pgcd
        seconddiv=o.denom/pgcd

        numT=self.num/firstdiv-o.num/seconddiv

        return str(numT)+"/"+str(pgcd)

    def __mul__(self,o):
        return str(self.num*o.num)+"/"+str(self.denom*o.denom)

    def __truediv__(self,o):
        return str(self.num*o.denom)+"/"+str(self.denom*o.num)

    def __repr__(self):
        return repr(self.affiche())


frac1=Fraction(4,5)
frac2=Fraction(4,10)

frac1.affiche()

print(frac1+frac2)
print(frac1-frac2)
print(frac1*frac2)
print(frac1/frac2)

repr(frac1)