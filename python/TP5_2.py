class CompteBancaire:
    def __init__(self,name,solde=0):
        self.nom=name
        self.solde=solde
    
    def retrait(self,k):
        self.solde=self.solde-k

    def depot(self,k):
        self.solde=self.solde+k
        
    def affiche(self):
        print("Le solde du compte de "+str(self.nom) +" est "+str(self.solde)+ " euros.")

    def __repr__(self):
        return repr(self.affiche())

cbjuju=CompteBancaire("julien")
cbjuju.depot(700)
cbjuju.affiche()
cbjuju.retrait(300)
cbjuju.affiche()