class Domino :
    def __init__(self,a,b):
        self.faceA=a
        self.faceB=b
    
    def affiche_points(self):
        print("face A: "+str(self.faceA)+", face B: "+str(self.faceB))
    
    def totale(self):
        return self.faceA+self.faceB

    def __repr__(self):
        return repr(self.affiche_points())

d=Domino(2,5)
d.affiche_points()
k=d.totale()
print(k)